# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

permissions = [
  [true, true, true],
  [true, true, false],
  [true, false, false],
  [false, false, false],
  [false, false, true],
  [false, true, true],
  [false, true, false],
  [true, false, true]
]

permissions.each do |read, write, trash|
  Permission.create!(read: read, write: write, trash: trash);
end

users = [
  ["Joe", "joe@joe.com", "pass123", 1],
  ["Mary", "Mary@Mary.com", "pass123", 2],
  ["Bill", "bill@bill.com", "pass123", 3],
  ["Liz", "liz@liz.com", "pass123", 4]
]

users.each do |name, email, password, permissions_id|
  User.create!(name: name, email: email, password: password,
    permissions_id: permissions_id);
end
