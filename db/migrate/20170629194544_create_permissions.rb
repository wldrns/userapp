class CreatePermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :permissions do |t|
      t.boolean :read
      t.boolean :write
      t.boolean :trash

      t.timestamps
    end
  end
end
