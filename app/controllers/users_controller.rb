class UsersController < ApplicationController
  def index
    @user_list = User.all
    @users_permissions_list = @user_list.map do |user|
      [user, Permission.find(user.permissions_id || 1)]
    end
    @user = User.new
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
    @permissions = Permission.find(@user.permissions_id)
  end

  def destroy
    @user = User.find(params[:id]).destroy
    redirect_to users_path
    flash[:notice] = 'User was deleted.'
  end

  def create
    permissions_id = map_permissions
    @user = User.new(user_params)
    @user.permissions_id = permissions_id
    @user.save!
    redirect_to users_path
    flash[:notice] = 'New user was created!'
  # TODO: get the form validation working
  # rescue ValidationError
  #   flash.now[:alert] = 'Error. Please fix your form.'
  #   render :index
  #   # raise
  end

  def update
    permissions_id = map_permissions
    @user = User.find(params[:id])
    @user.assign_attributes(user_params)
    @user.permissions_id = permissions_id
    @user.save!
    redirect_to users_path
    flash[:notice] = 'User was updated!'
  end

  private

  def user_params
    params.require(:user).permit(
      %i[name email password]
    )
  end

  def map_permissions
    read = params[:read] == 'on' ? true : false
    write = params[:write] == 'on' ? true : false
    trash = params[:trash] == 'on' ? true : false

    permissions_id = 0
    permissions_map = [
      [true, true, true],
      [true, true, false],
      [true, false, false],
      [false, false, false],
      [false, false, true],
      [false, true, true],
      [false, true, false],
      [true, false, true]
    ]

    params_arr = [read, write, trash]

    permissions_map.each_with_index  do |permission, index|
      permissions_id = index if permission == params_arr
    end

    permissions_id + 1
  end
end
