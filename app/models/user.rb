class User < ActiveRecord::Base
  has_one :permissions

  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true
end
